//
//  CarDatabaseTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 3/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation
import XCTest

@testable import PurpleCar

final class CarDatabaseTests: XCTestCase {
    
    func test_init() {
        let file = Bundle.main.url(forResource: "cars", withExtension: "db")!
        let keyManager = KeyManager(key: "0HXr7Ws5oOOy3iYhhCLCtmPvCluEJgX8")
        XCTAssertNoThrow(try CarDatabase(file: file, keyManager: keyManager))
    }
    
    func test_cars_returnsListOfCarsFromTheFile() {
        let file = Bundle.main.url(forResource: "cars", withExtension: "db")!
        let keyManager = KeyManager(key: "0HXr7Ws5oOOy3iYhhCLCtmPvCluEJgX8")
        let carDatabase = try? CarDatabase(file: file, keyManager: keyManager)
        let cars = carDatabase?.fetchCars()
        XCTAssertEqual(cars?.count, 4)
    }
    
    func generateCars() {
        let cars = [
            try! Car(carID: 1, vin: "5GZCZ43D13S812715", model: "Model S"),
            try! Car(carID: 2, vin: "5GZCZ43D13S812716", model: "Model 3"),
            try! Car(carID: 3, vin: "5GZCZ43D13S812717", model: "Model X"),
            try! Car(carID: 4, vin: "5GZCZ43D13S812718", model: "Model Y")
        ]
        let json = try! JSONEncoder().encode(cars)
        let key = random(length: 32)
        let encryptedCars = json.encryptAES256_CBC_PKCS7_IV(keyString: key)
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("cars.db")
        try! encryptedCars?.write(to: url)
    
        print("key", key)
        print("saved to", url)
    }
    
    func random(length n: Int = 10) -> String {
        let characters = Array("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        return (0..<n).map { _ in
            return String(characters[Int.random(in: (0..<characters.count))])
        }.joined()
    }
}
