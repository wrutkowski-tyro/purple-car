//
//  ModelTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import XCTest
@testable import PurpleCar

class ModelTests: XCTestCase {
    func testShouldAcceptModel() {
        XCTAssertNoThrow(try Model(value: "Model Y"))
    }
    
    func testShouldFailModel_empty() {
        XCTAssertThrowsError(try Model(value: ""), "") { (error) in
            XCTAssertEqual(error as? Model.ValidationError, .empty)
        }
    }
    
    func testShouldFailModel_tooShort() {
        XCTAssertThrowsError(try Model(value: "A"), "") { (error) in
            XCTAssertEqual(error as? Model.ValidationError, .tooShort)
        }
    }
    
    func testShouldFailModel_tooLong() {
        XCTAssertThrowsError(try Model(value: "Model 3 Premium Super Combo"), "") { (error) in
            XCTAssertEqual(error as? Model.ValidationError, .tooLong)
        }
    }

    func testShouldFailModel_incorrectCharacters() {
        XCTAssertThrowsError(try Model(value: "M0d3l $"), "") { (error) in
            XCTAssertEqual(error as? Model.ValidationError, .incorrectFormat)
        }
    }
}
