//
//  CarTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import XCTest
@testable import PurpleCar

class CarTests: XCTestCase {
    func testParsesJSONToCar() {
        let json = """
{"carID": 12345, "vin": "5GZCZ43D13S812715", "model": "Model Y"}
"""
        do {
            let car = try JSONDecoder().decode(Car.self, from: json.data(using: .utf8)!)
            XCTAssertNotNil(car)
        } catch {
            XCTFail("\(error)")
        }
    }
    
    func testFailParsingJSONToCar_wrongFormat() {
        let json = """
{"carID": "12345", "vin": "5GZCZ43D13S812715", "model": "Model Y"}
"""
        XCTAssertThrowsError(try JSONDecoder().decode(Car.self, from: json.data(using: .utf8)!), "")
    }
    
    func testFailParsingJSONToCar_missingData() {
        let json = """
{"carID": 12345, "model": "Tesla Model Y"}
"""
        XCTAssertThrowsError(try JSONDecoder().decode(Car.self, from: json.data(using: .utf8)!), "")
    }
    
    func testEncodesCarToJSON() {
        let expectedJson = """
{"carID":12345,"vin":"5GZCZ43D13S812715","model":"Model Y"}
"""
        let car = try! Car(carID: 12345, vin: "5GZCZ43D13S812715", model: "Model Y")
        let json = try! JSONEncoder().encode(car)
        XCTAssertEqual(json, expectedJson.data(using: .utf8))
    }
}
