//
//  VINTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import XCTest
@testable import PurpleCar

class VINTests: XCTestCase {
    func testShouldAcceptVIN() {
        XCTAssertNoThrow(try VIN(value: "5GZCZ43D13S812715"))
    }
    
    func testShouldFailVIN_empty() {
        XCTAssertThrowsError(try VIN(value: ""), "") { (error) in
            XCTAssertEqual(error as? VIN.ValidationError, .empty)
        }
    }

    func testShouldFailVIN_incorrectLength() {
        XCTAssertThrowsError(try VIN(value: "5GZCZ43D13S81271"), "") { (error) in
            XCTAssertEqual(error as? VIN.ValidationError, .incorrectLength)
        }
    }

    func testShouldFailVIN_incorrectFormat() {
        XCTAssertThrowsError(try VIN(value: "5GZCZ43D1-S812715"), "") { (error) in
            XCTAssertEqual(error as? VIN.ValidationError, .incorrectFormat)
        }
    }
}
