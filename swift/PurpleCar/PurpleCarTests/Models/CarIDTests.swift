//
//  CarIDTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import XCTest
@testable import PurpleCar

class CarIDTests: XCTestCase {
    func testShouldAcceptCarID() {
        XCTAssertNoThrow(try CarID(value: 12345))
    }
    
    func testShouldFailCarID_negative() {
        XCTAssertThrowsError(try CarID(value: -123), "") { (error) in
            XCTAssertEqual(error as? CarID.ValidationError, .negative)
        }
    }
    
    func testShouldFailCarID_tooBig() {
        XCTAssertThrowsError(try CarID(value: 1234567890), "") { (error) in
            XCTAssertEqual(error as? CarID.ValidationError, .tooBig)
        }
    }
}
