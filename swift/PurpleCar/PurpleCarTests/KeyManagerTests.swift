//
//  KeyManagerTests.swift
//  PurpleCarTests
//
//  Created by Wojciech Rutkowski on 3/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation
import XCTest

@testable import PurpleCar

final class KeyManagerTests: XCTestCase {
    
    func test_init() {
        let _ = KeyManager(key: "0xDEADBEEF")
    }
    
    func test_readKey_returnsKey() {
        let keyManager = KeyManager(key: "0xDEADBEEF")
        let key = try? keyManager.getKey()
        XCTAssertEqual(key, "0xDEADBEEF")
    }
    
    func test_readKeyAgain_returnsNil() {
        let keyManager = KeyManager(key: "0xDEADBEEF")
        let key = try? keyManager.getKey()
        XCTAssertThrowsError(try keyManager.getKey(), "") { (error) in
            XCTAssertEqual(error as? KeyManager.KeyManagerError, .accessingKeyMoreThanOnce)
        }
        XCTAssertEqual(key, "0xDEADBEEF")
    }
    
}
