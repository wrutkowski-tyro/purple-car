//
//  Car.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation

struct CarID {
    let value: Int
    
    init(value: Int) throws {
        try CarID.validate(carID: value)
        
        self.value = value
    }
    
    enum ValidationError: Error {
        case negative
        case tooBig
    }
    
    static func validate(carID: Int) throws {
        if carID < 0 { throw ValidationError.negative }
        if carID > 100000 { throw ValidationError.tooBig }
    }
}
