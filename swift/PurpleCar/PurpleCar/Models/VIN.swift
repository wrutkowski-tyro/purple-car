//
//  Car.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation

struct VIN {
    let value: String
    
    init(value: String) throws {
        try VIN.validate(vin: value)
        
        self.value = value
    }
    
    enum ValidationError: Error {
        case empty
        case incorrectLength
        case incorrectFormat
    }
    
    static func validate(vin: String) throws {
        if vin.isEmpty { throw ValidationError.empty }
        if vin.count != 17 { throw ValidationError.incorrectLength }
        guard vin ~= "^[A-Z0-9]*$" else { throw ValidationError.incorrectFormat }
    }
}
