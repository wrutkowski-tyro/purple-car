//
//  Car.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation

struct Car: Codable {
    let carID: CarID
    let vin: VIN
    let model: Model
    
    init(carID: Int, vin: String, model: String) throws {
        self.carID = try CarID(value: carID)
        self.vin = try VIN(value: vin)
        self.model = try Model(value: model)
    }
    
    enum CodingKeys: String, CodingKey {
        case carID, vin, model
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let carID = try container.decode(Int.self, forKey: .carID)
        let vin = try container.decode(String.self, forKey: .vin)
        let model = try container.decode(String.self, forKey: .model)
        try self.init(carID: carID, vin: vin, model: model)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(carID.value, forKey: .carID)
        try container.encode(vin.value, forKey: .vin)
        try container.encode(model.value, forKey: .model)
    }
}
