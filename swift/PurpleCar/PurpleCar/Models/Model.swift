//
//  Car.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 2/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation

struct Model {
    let value: String
    
    init(value: String) throws {
        try Model.validate(model: value)
        
        self.value = value
    }
    
    enum ValidationError: Error {
        case empty
        case tooShort
        case tooLong
        case incorrectFormat
    }
    
    static func validate(model: String) throws {
        if model.isEmpty { throw ValidationError.empty }
        if model.count <= 1 { throw ValidationError.tooShort }
        if model.count >= 20 { throw ValidationError.tooLong }
        guard model ~= "^[a-zA-Z0-9 ]*$" else { throw ValidationError.incorrectFormat }
    }
}
