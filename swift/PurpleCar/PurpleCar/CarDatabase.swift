//
//  CarDatabase.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 3/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation
import CommonCrypto

final class CarDatabase {
    
    enum CarDatabaseError: Error {
        case decryption
    }
    
    private var cars: [Car]
    
    init(file: URL, keyManager: KeyManager) throws {
        let carsEncrypted = try Data(contentsOf: file)
        guard let carsJSON = carsEncrypted.decryptAES256_CBC_PKCS7_IV(keyString: try keyManager.getKey()) else {
            throw CarDatabaseError.decryption
        }
        cars = try JSONDecoder().decode([Car].self, from: carsJSON)
    }
    
    func fetchCars() -> [Car] {
        return cars
    }
}
