//
//  KeyManager.swift
//  PurpleCar
//
//  Created by Wojciech Rutkowski on 3/3/20.
//  Copyright © 2020 Wojciech Rutkowski. All rights reserved.
//

import Foundation

final class KeyManager {
    
    private var key: String?
    
    init(key: String) {
        self.key = key
    }
    
    func getKey() throws -> String {
        guard let key = self.key else {
            throw KeyManagerError.accessingKeyMoreThanOnce
        }
        self.key = self.key.map { _ in "-" }
        self.key = nil
        return key
    }
    
    enum KeyManagerError: Error {
        case accessingKeyMoreThanOnce
    }
}
