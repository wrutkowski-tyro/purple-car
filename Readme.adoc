= Purple Car

//tag::abstract[]

You are part of the development team at Purple Car, an online car sale business
(nobody should ever walk). 
You will be given objectives during the course and
use this program as a base.

//end::abstract[]

//tag::lab[]

== Build and run

*Fork* and clone *your forked repository*.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: Tests will be incomplete.

//end::lab[]

//tag::references[]

//end::references[]
