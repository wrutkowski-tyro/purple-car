package app;

public class Car {

    private Integer carID;
    private String vin;
    private String model;
    //private String plateNo;
    //private File photo;
    
    public Car() {
        super();
    }

    public Car(Integer carID, String vin, String model) {
        this.carID = carID;
        this.vin = vin;
        this.model = model;
    }

    public Integer getcarID() {
        return this.carID;
    }

    public String getvin() {
        return this.vin;
    }
    
    public String getmodel() {
        return this.model;
    }

    public void setCarID(Integer value) {
        this.carID = value;
    }

    public void setVin(String value) {
        this.vin = value;
    }

    public void setModel(String value) {
        this.model = value;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.model;
    }

}
