package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarSaleController {

    @RequestMapping("/")
    String render() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String render(@RequestBody Car car) {
        return "Added car: " + car.toString();
    }
}
